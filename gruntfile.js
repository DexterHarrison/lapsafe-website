module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
        scripts: {
            files: ['lib/sass/*.scss', 'lib/js/*/*.js'],
            tasks: ['sass', 'cssmin', 'uglify'],
        }
    },

    sass: {
        dist: {
            files: {
                'Site/css/style.css' : 'lib/sass/base.scss'
            }
        }
    },

    cssmin: {
        target: {
            files: {
                'Site/css/style.min.css': 'Site/css/style.css'
            }
        }
    },

    uglify: {
        options: {
            mangle: false
        },
        my_target: {
            files: {
                'Site/js/testimonials.min.js': ['lib/js/testimonials/*.js']
            }
        }
    },

  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);

};
