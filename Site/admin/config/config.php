<?php

    include(__DIR__.'/config.local.php');  

    define('PERCH_LICENSE_KEY', 'R21410-PWH698-BZP753-JWE185-HCG029');
    define('PERCH_EMAIL_FROM', 'webmaster@lapsafe.com');
    define('PERCH_EMAIL_FROM_NAME', 'LapSafe Products');

    define('PERCH_LOGINPATH', '/admin');
    define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', __DIR__));
    define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');

    define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'resources');
    define('PERCH_RESPATH', PERCH_LOGINPATH . '/resources');
    
    define('PERCH_HTML5', true);
    define('PERCH_TZ', 'Europe/London');

    define('PERCH_RWD', true);
    define('PERCH_CLEAN_RESOURCES', false);