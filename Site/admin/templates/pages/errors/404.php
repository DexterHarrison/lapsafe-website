<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <ul class="breadcrumbs">
            <li class="home"><img src="/images/icons/home.jpg"></li>
            <li>Page not found</li>
        </ul>
        
        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <h2>Sorry that page does not exist</h2>
                </div>
            </div>
        </div>            
    
        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>