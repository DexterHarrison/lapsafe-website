<!DOCTYPE html>
<html>
    <head>
		<?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
            perch_pages_breadcrumbs();
        ?>
        
        <?php 
            perch_content_create('Sub Title', array(
                'template' => 'General/Text.html',
            ));
            perch_content_create('Introduction', array(
                'template' => 'General/Text_Block.html',
            ));
        ?>
        
        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <h2><?php perch_content_custom('Sub Title'); ?></h2>
                    <?php perch_content('Introduction'); ?>
                </div>
            </div>
        </div>
        
        <?php perch_layout('global/footer'); ?>
    </body>
</html>