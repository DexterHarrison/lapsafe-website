<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
        <style>
            
            #tabletOptions {
                display: none;
            }
            
            .container .offset-by-half                   { padding-left: 30px;  }
            
            .filterOption {
                background: #dfccd5;
                border-radius: 6px;
                height: 160px;
                box-sizing: border-box;
                transition: 0.5s background;
                cursor: pointer;
            }
            
            .filterOption img {
                width: 75%;
                display: block;
                margin: 0 auto;
                padding-top: 10px;
            }
            
            .filterOption span {
                margin-top: 15px;
                display: block;
                text-align: center;
                font-size: 18px;
                color: #9a4264;
                font-weight: 600;
                transition: 0.5s color;
            }
            
            .filterOption h3 {
                font-size: 32px;
                font-weight: 600;
                line-height: 160px;
                color: #9a4264;
                transition: 0.5s color;
            }
            
            input[type=checkbox]:checked + .filterOption {
                background: #9a4264;
            }
            
            input[type=checkbox]:checked + .filterOption span, input[type=checkbox]:checked + .filterOption h3 {
                color: white;
            }
            
            input[type=checkbox] {
                display: none;
            }
            
            .nextSection {
                margin: 20px auto 0 auto;
                width: 50px;
                height: 50px;
                border-radius: 50%;
                background: url('/images/icons/devices/arrow.png') no-repeat center white;
                border: 1px solid #dfccd5;
            }
            
            .formButton {
                font-size: 16px;
                background: #9a4264;
                padding: 20px;
                border: none;
                border-bottom: 6px solid #6b0035;
                color: white;
                font-weight: 600;
                text-align: center;
                margin: 20px auto 0 auto;
                display: block;
            }
            
            .formButton:active {
                border-bottom: none;
                border-top: 6px solid #eee2e8;
            }

        </style>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <?php perch_pages_breadcrumbs(); ?>
        
        <?php

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $cats = array();
            
            if ($_POST['chromebooks']) {
                $cats[] = 'devices/chromebooks';
            }
            
            if ($_POST['laptops']) {
                $cats[] = 'devices/laptops';
            }

            if ($_POST['ipads']) {
                $cats[] = 'devices/ipads';
            }

            if ($_POST['tablets']) {
                $cats[] = 'devices/tablets';
            }

            if ($_POST['ereaders']) {
                $cats[] = 'devices/ereaders';
            }

            
            $filters = array();

            // Qty?
            if($_POST['10']) {
                $qty = '1,10';
            } elseif($_POST['20']) {
                $qty = '11,20';   
            } elseif($_POST['30']) {
                $qty = '21,30';   
            } elseif($_POST['31']) {
                $qty = '31,100';
            } else { $qty = '0,100'; }
                       
            $filters[] = array (
                'filter'    => 'qty',
                'match'     => 'between',
                'value'     => $qty
            );
            
            // Product Type?
            if($_POST['trolley']) {
                $filters[] = array (
                    'filter' => 'productType',
                    'match' => 'eq',
                    'value' => 'trolley'
                );
            } elseif($_POST['case']) {
                $filters[] = array (
                    'filter' => 'productType',
                    'match' => 'eq',
                    'value' => 'case'
                );
            }
            
            // Require Sync?
            if($_POST['sync']) {
                $filters[] = array (
                    'filter' => 'charging',
                    'match' => 'eq',
                    'value' => 'charge-sync'
                );
            } 
            
            // Cases?
            if($_POST['slim']) {
                $filters[] = array (
                    'filter' => 'cases.title',
                    'match' => 'eq',
                    'value' => 'slim'
                );
            } elseif($_POST['ruggedised']) {
                $filters[] = array (
                    'filter' => 'cases.title',
                    'match' => 'eq',
                    'value' => 'ruggedised'
                );
            } elseif($_POST['gripcase']) {
                $filters[] = array (
                    'filter' => 'cases.title',
                    'match' => 'eq',
                    'value' => 'gripcase'
                );   
            }
                
            perch_collection('Products', [
                'template' => '_Portal/Table.html',
                'category-match' => 'all',
                'category' => $cats,
                'filter' => $filters
            ]);
                
            } else {
                perch_form('filter.html');
            }       

            
    
        ?>
    
        <script>

        $('.tablets').click(function() {
    if( $('.tablets:checked').length > 0 ) {
        $("#tabletOptions").fadeIn();
    } else {
        $("#tabletOptions").fadeOut();
    }
}); 
            
            $('input.qty').each(function() {
    $(this).on('touchstart click', function() {
    $('input.qty').not(this).removeAttr('checked');
});
});
            
            $('input.type').each(function() {
    $(this).on('touchstart click', function() {
    $('input.type').not(this).removeAttr('checked');
});
});
            
            $('input.sync').each(function() {
    $(this).on('touchstart click', function() {
    $('input.sync').not(this).removeAttr('checked');
});
});
            $('input.cases').each(function() {
    $(this).on('touchstart click', function() {
    $('input.cases').not(this).removeAttr('checked');
});
});
            
        function uncheckAll(){
            $("input[type='checkbox']:checked").prop("checked",false);
            $("#tabletOptions").fadeOut();
        }
            
        </script>

        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>