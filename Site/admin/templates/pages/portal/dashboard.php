

<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        <?php
            perch_collection('Products', array(
                'template' => '_Products/_Portal_Product_Listing.html'
            ));
        ?>
    </body>
</html>