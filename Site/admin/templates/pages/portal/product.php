<?php
    include ($_SERVER['DOCUMENT_ROOT'].'/includes/db.php');

    // Declare the class instance
    $db = new Sparrow();

    $db->setDb('pdomysql://root:root@localhost/lapsafe');

    $product = perch_get('p');
    
    echo $product;

    $rows = $db->from('products')
        ->where('productRange =', $product)
        ->where('productType =', 'Product')
        ->many();

    $total = count($rows);
    if ($total !== 0) {
        echo "<h2>Products</h2>";
        echo "<table class='u-full-width'><thead><tr>";
        echo "<th>Product Code</th>";
        echo "<th>Product Name</th>";
        echo "<th>BP Price</th>";
        echo "<th>RRP Price</th>";
        echo "</tr></thead>";
        foreach($rows as $row) {
            echo "<tr>";
            echo "<td>".$row->productCode."</td>";
            echo "<td>".$row->productName."</td>";
            echo "<td>£".$row->BPPrice."</td>";
            echo "<td>£".$row->RRPPrice."</td>";
            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "No Results";   
    }
?>