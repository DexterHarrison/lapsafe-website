<!doctype html>
<html lang="en">
    <head>
		<?php perch_layout('global/masthead'); ?>
    </head>
    <body>
        <?php perch_layout('global/nav'); ?>
        
        <ul class="breadcrumbs">
            <li class="home"><a href="/"><img src="/images/icons/home.jpg"></a></li>
            <li>Search</li>
            <li><?php $query = perch_get('q'); echo "$query" ?></li>
        </ul>
        
        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <?php
                        if(!empty(perch_get('q'))) {
                            perch_content_search($query, array(
                                'count'=>5,
                                'no-conflict'=>true,
                                'hide-default-doc'=>true,
                                'hide-extensions' => true,
                                'apps'=>array('PerchContent', 'PerchEvents')
                            ));
                        } else {
                            perch_search_form();
                        }
                    ?>
                    
                    <?php  ?>
                </div>
            </div>
        </div>
        
    </body>
</html>