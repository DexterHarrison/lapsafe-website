<!DOCTYPE html>
<html>

  <head>
    <?php perch_layout('global/masthead'); ?>
  </head>

  <body class="home">

    <?php
      perch_layout('global/nav');
      perch_content('Slider');
    ?>

    <section class="intro">
      <div class="container">
        <div class="hlfrow">
          <div class="four columns offset-by-one">
            <img class="centerImg" width="250px" style="padding: 10px;" src="images/logos/footer-logo.png">
          </div>
          <div class="ten columns">
            <h1><strong>Powering</strong> Smart Technologies</h1>
          </div>
        </div>
        <?php perch_content('Introduction Text'); ?>
      </div>
    </section>

    <?php
      perch_content('Charging Trolleys');
      perch_content('Charge & Sync');
      perch_content('Charging Lockers');
    ?>

    <section id="testimonials">
      <?php
        perch_collection('Testimonials', [
		      'sort-order' =>'RAND',
		      'count'      =>6
        ]);

        perch_collection('Testimonials', [
          'template' => 'Testimonials/_All_Testimonial.html'
        ]);
      ?>
    </section>

    <?php perch_layout('global/footer'); ?>

    <script type="text/javascript" src="/js/testimonials.min.js"></script>
    <script>
      $(window).load(function() {
        $('.lapsafe-slider').flexslider({
          animation: "slide",
          startAt: 0,
          directionNav: false,
          namespace: "lapsafe-",
        });
      });
    </script>

  </body>

</html>
