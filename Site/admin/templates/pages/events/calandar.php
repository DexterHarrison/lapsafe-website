<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
            <?php perch_pages_breadcrumbs(); ?>
		
		
            <div class="page">
                
                <div class="container">
                    <div class="expo-cal">
                        <div class="sixteen columns">
							<?php
								$dates = perch_get('date').'01 00:00:00, '.perch_get('date').'-31 23:59:59';
								perch_events_calendar(array(
									'filter'   => 'eventDateTime',
									'match'    => 'eqbetween',
									'value'    => $dates,
								));
							?>
                        </div>
                    </div>
					
                    <!--[if lt IE 9]>
                        <?php $opts = array(
                                'past-events'=>false,
                                'template'=>'events/listing/expo.html'
                            );
                        ?>
                        <?php perch_events_custom($opts); ?>
                    <![endif]-->
                </div>
                
                <div class="mobile-cal">
                    <div class="container">
                        <div class="sixteen columns">
				            <?php perch_events_listing(); ?>
                        </div>
                    </div>
                </div>
                
            </div>
        
            <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>