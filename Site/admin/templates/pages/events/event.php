<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/head'); ?>
		<?php perch_events_custom(array(
            'filter'=>'eventSlug',
            'match'=>'eq',
            'value'=>perch_get('event'),
            'template'=>'event_meta.html'
            ));
        ?>
    </head>
    
    <body>
            
           <?php perch_layout('global/nav'); ?>
        
            <?php perch_events_custom(array(
                'filter'=>'eventSlug',
                'match'=>'eq',
                'value'=>perch_get('event'),
            )); ?>
            <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>