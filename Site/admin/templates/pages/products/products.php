<!DOCTYPE html>
<html>

  <head>
    <?php perch_layout('global/masthead'); ?>
  </head>

  <body>

    <?php perch_layout('global/nav'); ?>

    <?php

      perch_content_create('Header', array(
        'template' => 'General/_Title_Header.html',
      ));

      perch_content_custom('Header');

      perch_pages_breadcrumbs();

    ?>

    <div class="page">
      <div class="container">
        <div class="sixteen columns">
          <h2><?php perch_content('Sub Title'); ?></h2>
          <p><?php perch_content('Introduction'); ?></p>
        </div>
      </div>
    </div>

        <?php
            perch_collection('Products');
        ?>

        <?php perch_layout('global/footer'); ?>


    </body>

</html>
