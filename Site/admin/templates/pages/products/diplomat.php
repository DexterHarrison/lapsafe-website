<?php
    $url = basename($_SERVER['REQUEST_URI']);
    $keys = parse_url($url); // parse url
    $path = explode("/", $keys['path']); // split path
    $slug = end($path); // slug
?>
<?php // Create Regions
    perch_content_create('Header', array(
        'template'  =>  '_Products/_Product_Header.html',
    ));

    perch_content_create('Overview', array(
        'template'  =>  '_Products/_Diplomat_Information.html',
    ));

    perch_content_create('Charging Options / Intelligent Systems', array(
        'template'  =>  '_Products/_Charging_Options.html',
        'multiple'  =>  true,
        'edit-mode' =>  'listdetail',
        'columns'   =>  'title, image',
    ));

    if($slug !== 'cam') {
        perch_content_create('Features / Benefits', array(
            'template'  => '_Products/_parallax.html',
            'multiple'  => false,
            'columns'   => 'title',
        ));
    }

    perch_content_create('Tabs', array(
        'template'  =>  'Misc/_Tabs.html',
    ));

    if($slug == 'lms') {
        perch_content_create('Video', array(
            'template'  => 'General/Video.html',
        ));
        perch_content_create('Testimonials', array(
            'template' => 'Testimonials/_Slideshow_Testimonial.html',
        ));
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>

    <body class="productPage">

        <?php perch_layout('global/nav'); ?>

        <?php perch_content_custom('Header'); ?>


        <?php
            perch_pages_breadcrumbs();
            perch_content_custom('Overview');
            perch_content_custom('Charging Options / Intelligent Systems');
            if($slug !== 'cam') {
                perch_content_custom('Features / Benefits');
            }
        ?>
        <div class="tabContainer">
            <?php
                perch_content_custom('Tabs', array(
                    'template' => 'Misc/_Tabs-Tab.html',
                ));
                perch_content_custom('Tabs');
            ?>
        </div>

        <?php
            if($slug == 'lms') {
                perch_content('Video');
                echo '<section id="testimonials">';
                perch_content_custom('Testimonials');
                perch_content_custom('Testimonials', array(
                    'template' => 'Testimonials/_All_Testimonial.html',
                ));
                echo '</section>';
            }
        ?>
        <section class="page" id="gallery">
            <div class="container">
                <div class="sixteen columns row">
                    <h2>Image Gallery</h2>
                </div>
                <?php perch_gallery_album_images($slug, array(
                    'template' => 'lightbox.html',
                )); ?>
            </div>
        </section>

        <?php perch_layout('cta'); ?>

        <?php perch_layout('global/footer'); ?>

        <script type="text/javascript" src="/js/testimonials.min.js"></script>
        <script type="text/javascript" src="/js/lightbox.min.js"></script>
        <script>
            $(document).ready(function(){
                $('a[href^="#"]').on('click',function (e) {
                    e.preventDefault();

                    var target = this.hash,
                    $target = $(target);

                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top-60
                    }, 900, 'swing', function () {
                    window.location.hash = target;
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function(){

	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})
        </script>

    </body>
</html>
