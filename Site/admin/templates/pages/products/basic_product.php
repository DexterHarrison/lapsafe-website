<?php
    $url = basename($_SERVER['REQUEST_URI']);
    $keys = parse_url($url); // parse url
    $path = explode("/", $keys['path']); // split path
    $slug = end($path); // slug
?>

<?php
    perch_content_create('Header', array(
        'template'  =>  '_Products/_Product_Header.html',
    ));
    perch_content_create('Overview', array(
        'template'  =>  '_Products/_Trolley_Information.html',
    ));
    if($slug == 'charge-and-sync') {
        perch_content_create('Options', array(
            'template'  =>  '_Products/_Charging_Options.html',
            'multiple'  =>  true,
            'edit-mode' =>  'listdetail',
            'columns'   =>  'title, image',
        ));
    }
    perch_content_create('Weights & Dimensions', array(
        'template'  =>  '_Products/_table.html',
        'multiple'  =>  true,
        'edit-mode' =>  'listdetail',
        'columns'   =>  'product-name',
    ));
?>

<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>

    <body class="productPage">

        <?php perch_layout('global/nav'); ?>

        <?php
            perch_content_custom('Header');
        ?>

        <?php
            perch_pages_breadcrumbs();
            perch_content_custom('Overview');
            if($slug == 'charge-and-sync') {
                perch_content_custom('Options');
            }
            perch_content_custom('Weights & Dimensions');
        ?>

        <section class="page" id="gallery">
            <div class="container">
                <div class="sixteen columns row">
                    <h2>Image Gallery</h2>
                </div>
                <?php perch_gallery_album_images($slug, array(
                    'template' => 'lightbox.html',
                )); ?>
            </div>
        </section>

        <?php perch_layout('cta'); ?>

        <?php perch_layout('global/footer'); ?>

        <script type="text/javascript" src="/js/lightbox.min.js"></script>
        <script>
            $(document).ready(function(){
                $('a[href^="#"]').on('click',function (e) {
                    e.preventDefault();

                    var target = this.hash,
                    $target = $(target);

                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top-60
                    }, 900, 'swing', function () {
                    window.location.hash = target;
                    });
                });
            });
        </script>

    </body>
</html>
