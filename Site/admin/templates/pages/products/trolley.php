<?php
    $url = basename($_SERVER['REQUEST_URI']);
    $keys = parse_url($url); // parse url
    $path = explode("/", $keys['path']); // split path
    $slug = end($path); // slug
?>

<?php // Create Regions
    perch_content_create('Header', array(
        'template'  =>  '_Products/_Product_Header.html',
    ));

    perch_content_create('Overview', array(
        'template'  =>  '_Products/_Trolley_Information.html',
    ));

    perch_content_create('Charging Options', array(
        'template'  =>  '_Products/_Charging_Options.html',
        'multiple'  =>  true,
        'edit-mode' =>  'listdetail',
        'columns'   =>  'title, image',
    ));

    perch_content_create('Options', array(
        'template'  =>  '_Products/_Additional_Options.html',
        'multiple'  =>  true,
        'edit-mode' =>  'listdetail',
        'columns'   =>  'title, image',
    ));

    perch_content_create('Weights & Dimensions', array(
        'template'  =>  '_Products/_table.html',
        'multiple'  =>  true,
        'edit-mode' =>  'listdetail',
        'columns'   =>  'product-name',
    ));

    perch_content_create('Rollover', array(
        'template'  =>  '_Products/_Rollover.html',
    ));

    perch_content_create('Range', array(
        'template'  =>  '_Products/_Range.html',
    ));
?>

<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>

    <body class="productPage">
        <?php perch_layout('global/nav'); ?>

        <?php perch_content_custom('Header'); ?>

        <?php
            perch_pages_breadcrumbs();
            perch_content_custom('Overview');
            perch_content_custom('Charging Options');
            perch_content_custom('Options');
            if($slug !== 'mentor' && $slug !== 'solo-plus-plus') {
				echo '<div id="colours" class="page">';
				echo '<div class="container">';
				echo '<div class="sixteen columns">';
				perch_content('Colours');
				echo '</div></div></div>';
			}
            perch_content_custom('Weights & Dimensions');
            perch_content_custom('Rollover');
            perch_content_custom('Range');
		?>

        <section class="page" id="gallery">
            <div class="container">
                <div class="sixteen columns row">
                    <h2>Image Gallery</h2>
                </div>
                <?php perch_gallery_album_images($slug, array(
                    'template' => 'lightbox.html',
                )); ?>
            </div>
        </section>

        <?php perch_layout('cta'); ?>

        <?php perch_layout('global/footer'); ?>

        <script type="text/javascript" src="/js/lightbox.min.js"></script>
        <script>
            $(document).ready(function(){
                $('a[href^="#"]').on('click',function (e) {
                    e.preventDefault();

                    var target = this.hash,
                    $target = $(target);

                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top-60
                    }, 900, 'swing', function () {
                    window.location.hash = target;
                    });
                });
            });
        </script>

    </body>
</html>
