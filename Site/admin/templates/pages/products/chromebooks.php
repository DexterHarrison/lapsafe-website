<!DOCTYPE html>
<html>
    <head>
		<?php perch_layout('global/masthead'); ?>
        <link href='http://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>
        <style>

            .stats tbody tr:first-of-type {
                font-weight:400;
            }

            .stats thead tr .header {
	           background-image: url(/images/icons/table/sort.png);
	           background-repeat: no-repeat;
	           background-position: center right;
	           cursor: pointer;
            }

            .stats thead tr .headerSortUp {
	           background-image: url(/images/icons/table/asc.png);
            }

            .stats thead tr .headerSortDown {
	           background-image: url(/images/icons/table/desc.png);
            }

            .stats .hide {
                display: none;
            }

        </style>
    </head>

    <body class="chromebooks">
        <?php perch_layout('global/nav'); ?>

        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>

        <?php perch_pages_breadcrumbs(); ?>

        <div class="page">

            <div class="container row clearfix">
                <div class="sixteen columns">
                    <h2><?php perch_content('Sub Title'); ?></h2>
                </div>
                <div class="nine columns">
                    <?php perch_content('Introduction'); ?>
                </div>
                <div class="seven columns">
                    <?php perch_content('Chromebook Image'); ?>
                </div>
            </div>

            <section id="chromebookComparison">
                <div class="container">
                    <div class="one-third column">
                        <div class="comparison green">
                            <div class="heading">
                                <span>Tier 1</span>
                                <div class="productName">
                                    <span>ClassBuddy&trade;</span>
                                </div>
                            </div>
                            <div class="points">
                                <ul>
                                    <li class="chromebook">Up to 30 Chromebooks</li>
                                    <li class="cost">Cost-effective Solution</li>
                                    <li class="footprint">Small footprint</li>
                                </ul>
                            </div>
                            <a href="/products/mobile-charging/classbuddy">ClassBuddy Chromebook Charging Trolley</a>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="comparison blue">
                            <div class="heading">
                                <span>Tier 2</span>
                                <div class="productName">
                                    <span>UnoCart&trade;</span>
                                </div>
                            </div>
                            <div class="points">
                                <ul>
                                    <li class="chromebook">Up to 30 Chromebooks</li>
                                    <li class="fan">Thermostatically controlled fans</li>
                                    <li class="wheel">Light & Easy to Manoeuvre</li>
                                    <li class="doors">270&deg; Fold Back Doors</li>
                                </ul>
                            </div>
                            <a class="blue" href="/products/mobile-charging/unocart">UnoCart Chromebook Charging Trolley</a>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="comparison red">
                            <div class="heading">
                                <span>Tier 3</span>
                                <div class="productName">
                                    <span>Mentor&trade;</span>
                                </div>
                            </div>
                            <div class="points">
                                <ul>
                                    <li class="chromebook">Up to 60 Chromebooks</li>
                                    <li class="secure">Most Secure</li>
                                    <li class="power">Ultra-Safe Power Management</li>
                                    <li class="smart">Smart Charging</li>
                                    <li class="ac">No AC Adaptors</li>
                                </ul>
                            </div>
                            <a class="green" href="/products/mobile-charging/mentor">Mentor Chromebook Charging Trolley</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <h2><?php perch_content('Chromebook Table Title'); ?></h2>
                    <?php perch_content('Chromebook Table Introduction'); ?>
                    <?php perch_content('Chromebook Table'); ?>
                    <?php perch_content('Chromebook Table Download'); ?>
                </div>
            </div>
        </div>

        <?php perch_layout('global/footer'); ?>
        <script type="text/javascript" src="/js/tablesorter.min.js"></script> 
        <script>
            $(document).ready(function() {
                $("#deviceTable").tablesorter();
             });
        </script>
    </body>

</html>
