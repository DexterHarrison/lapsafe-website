<?php
    $url = basename($_SERVER['REQUEST_URI']);
    $keys = parse_url($url); // parse url
    $path = explode("/", $keys['path']); // split path
    $slug = end($path); // slug
?>

<!DOCTYPE html>
<html>
    <head>
		<?php perch_layout('global/masthead'); ?>
    </head>

    <body>

        <?php perch_layout('global/nav'); ?>

        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>

        <?php perch_pages_breadcrumbs(); ?>

        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <h2><?php perch_content('Sub Title'); ?></h2>
                    <?php perch_content('Introduction'); ?>
                </div>
            </div>
        </div>

        <?php perch_content('Ranges'); ?>

        <?php perch_layout('global/footer'); ?>


    </body>

</html>
