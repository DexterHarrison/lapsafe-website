<?php // Jiggery to get Dynamic product ID based on last part of page URL
    $url = basename($_SERVER['REQUEST_URI']); // Get URL
    $keys = parse_url($url); // parse the url
    $path = explode("/", $keys['path']); // splitting the path
    $catID = end($path); // get the value of the last element
?>

<!DOCTYPE html>
<html>
    <head>
		<?php perch_layout('global/masthead'); ?>
    </head>

    <body>

        <?php perch_layout('global/nav'); ?>

        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>

        <?php perch_pages_breadcrumbs(); ?>

        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <h2><?php perch_content('Sub Title'); ?></h2>
                    <?php perch_content('Introduction'); ?>
                </div>
            </div>
        </div>

        <?php
            perch_collection('Products', array(
                'category'=>'devices/'.$catID,
            ));
        ?>

        <?php perch_layout('global/footer'); ?>

    </body>

</html>
