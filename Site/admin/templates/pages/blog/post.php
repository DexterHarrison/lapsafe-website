<?php perch_blog_check_preview(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
            $domain        = 'http://'.$_SERVER["HTTP_HOST"];
            $url           = $domain.$_SERVER["REQUEST_URI"];
            $sitename      = "LapSafe Products - Store, Charge & Sync";
            $twittername   = "@lapsafe";
            $title = perch_blog_post_field(perch_get('s'), 'postTitle', true);
            $desc = strip_tags(perch_blog_post_field(perch_get('s'), 'excerpt', true));
            $img = perch_blog_post_field(perch_get('s'), 'headerImage', true);

            perch_page_attributes_extend(array(
                'sitename'       => $sitename,
                'domain'         => $domain,
                'url'            => $url,
                'title'          => $title,
                'description'    => $desc,
                'sharing_image'  => $img,
                'og_title'       => $title,
                'og_description' => $desc,
                'og_type' => 'website'
            ));

            perch_page_attributes(array(
                'template'      => 'default.html'
            ));
        ?>
        <?php perch_layout('global/head'); ?>
        <script type="text/javascript" src="/js/smallslider.min.js"></script>
        <script>
            $(function(){
                $("#slides").slidesjs({
                    width: 700,
                    height: 400,
                    navigation: false,
                    interval: 5000,
                    auto: true
                });
            });
        </script>
    </head>

	<body>
		<?php perch_layout('global/nav'); ?>

        <ul class="breadcrumbs nohead">
            <li class="home"><img src="/images/icons/home.jpg"></li>
            <li><a href="/blog/">Blog</li></a>
            <li><?php echo $title; ?></li>
        </ul>

        <div id="articles" class="clearfix">
            <div id="articleList">
                <?php
                    $year = perch_get('year');
                    $dateFrom = $year.'-01-01';
                    $dateTo = $year.'-12-31';
                    perch_blog_custom(array(
                        'template' => 'post.html',
                        'filter' => 'postSlug',
                        'match' => 'eq',
                        'value' => perch_get('s')
                    ));
                ?>
            </div>
            <div id="articleSideBar">
                <?php perch_blog_post_categories(perch_get('s')); ?>
                <?php
                    perch_blog_custom(array(
                        'template' => 'post_sidebar.html',
                        'filter' => 'postSlug',
                        'match' => 'neq',
                        'value' => perch_get('s'),
                        'count' => 3,
                        'sort' => 'postDateTime',
                        'sort-order' => 'DESC',
                    ));
                ?>
                <div class="boxed clearfix sidebarContent blogItem">
                    <h3>Latest Tweets</h3>
                    <?php perch_twitter_get_latest(array(
                        'twitter_id' => 'lapsafe',
                        'template'=>'twitter/blog_tweet.html',
                        'count'=>4,
                    )); ?>
                </div>
            </div>
        </div>



        <?php perch_layout('global/footer'); ?>
</body>
</html>
