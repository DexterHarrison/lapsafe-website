<!DOCTYPE html>
<html lang="en">
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
	<body>
		<?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <?php perch_pages_breadcrumbs(); ?>
        
        <div id="articles" class="clearfix">
            
            <?php
                PerchSystem::set_var('myurl', '/blog/page/');
                perch_blog_custom(array(
                    'count' => 5,
                    'template' => 'post_listing.html',
                    'sort' => 'postDateTime',
                    'sort-order' => 'DESC',
                )); 
            ?>
            
            <div id="articleSideBar">
                <?php perch_blog_categories(array(
                    'sort' =>'catTitle',
                    'sort-order' => 'ASC',
                )); ?>
                <?php perch_blog_date_archive_years(); ?>
                <div class="boxed clearfix sidebarContent blogItem">
                    <h3>Latest Tweets</h3>
                    <?php perch_twitter_get_latest(array(
                        'twitter_id' => 'lapsafe',
                        'template'=>'twitter/blog_tweet.html',
                        'count'=>16,
                    )); ?>
                </div>
            </div>
            
        </div>
			
        <?php perch_layout('global/footer'); ?>

</body>
</html>