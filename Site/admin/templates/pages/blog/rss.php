<?php 
    $domain = 'http://'.$_SERVER['HTTP_HOST'];
    PerchSystem::set_var('domain', $domain);

    header('Content-Type: application/rss+xml');

    echo '<'.'?xml version="1.0"?'.'>'; 
?>

<rss version="2.0">
  <channel>
    <title>The LapSafe® Blog | Store Charge &amp; Sync</title>
    <link>http://example.com/</link>
    <description>The industry’s leader in powering smart technologies . We are dedicated in providing safe, secure and innovative solutions for laptops, iPads, tablets and other such devices in volume.</description>
    <?php
        perch_blog_custom(array(
            'template'=>'rss_post.html',
            'count'=>10,
            'sort'=>'postDateTime',
            'sort-order'=>'DESC'
        ));
    ?>
  </channel>
</rss>