<!DOCTYPE html>
<html lang="en">
    <head>
		<?php
            $domain        = 'http://'.$_SERVER["HTTP_HOST"];
            $url           = $domain.$_SERVER["REQUEST_URI"];
            $sitename      = "LapSafe Products - Store, Charge & Sync";
            $twittername   = "@lapsafe";
			if (perch_get('cat')) {
				$title = "LapSafe Blog Category: ".perch_blog_category(perch_get('cat'), true);
			} elseif (perch_get('year')) {
				$title = "LapSafe Blog Archive by Year: ".perch_get('year');
			}
            $desc = "Blah Blah";
            $img = "Blah";

            perch_page_attributes_extend(array(
                'sitename'       => $sitename,
                'domain'         => $domain,
                'url'            => $url,
                'title'          => $title,
                'description'    => $desc,
                'sharing_image'  => $img,
                'og_title'       => $title,
                'og_description' => $desc
            ));

            perch_page_attributes(array(
                'template'      => 'default.html'
            ));
        ?>
        <?php perch_layout('global/head'); ?>
    </head>
    <body>
        <?php perch_layout('global/nav'); ?>
        
        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>
        
        
        <?php
        $catmarkup = "<ul class='breadcrumbs'>";
        $catmarkup .= "<li class='home'><a href='/'><img src='/images/icons/home.jpg'></a></li>";
        $catmarkup .= "<li><a href='/blog/'>Blog</a></li>";
        $catmarkup .= "<li>Category</li>";
        $catmarkup .= "<li>".perch_blog_category(perch_get('cat'), true)."</li>";
        $catmarkup .= "</ul>";
        $catmarkup .= "<div class='page'><div class='container'><div class='sixteen columns'><h2>Category: ".perch_blog_category(perch_get('cat'), true)."</h2></div></div></div>";
        $catmarkup .= "<div id='articles' class='clearfix'>";

        $yearmarkup .= "<ul class='breadcrumbs'>";
        $yearmarkup .= "<li class='home'><a href='/'><img src='/images/icons/home.jpg'></a></li>";
        $yearmarkup .= "<li><a href='/blog/'>Blog</a></li>";
        $yearmarkup .= "<li>Year</li>";
        $yearmarkup .= "<li>".perch_get('year')."</li>";
        $yearmarkup .= "</ul>";
        $yearmarkup .= "<div class='page'><div class='container'><div class='sixteen columns'><h2>All LapSafe® Blog Posts in ".perch_get('year')."</h2></div></div></div>";
        $yearmarkup .= "<div id='articles' class='clearfix'>";

        ?>
        
            <?php
                // defaults for all modes
                $posts_per_page = 5;
                $template 		= 'post_listing.html';
                $sort_order		= 'DESC';
                $sort_by		= 'postDateTime';

                // Have we displayed any posts yet?
                $posts_displayed = false;

                /* --------------------------- POSTS BY CATEGORY --------------------------- */
                if (perch_get('cat')) {
                    echo $catmarkup;
                    PerchSystem::set_var('myurl', '/blog/category/'.perch_get('cat').'/page/');
                    perch_blog_custom(array(
                        'category'   => perch_get('cat'),
                        'template'   => $template,
                        'count'      => $posts_per_page,
                        'sort'       => $sort_by,
                        'sort-order' => $sort_order,
                    ));
                    
                    $posts_displayed = true;
                }


                    /* --------------------------- POSTS BY DATE RANGE --------------------------- */
                    if (perch_get('year')) {

                        $year              = intval(perch_get('year'));
                        $date_from         = $year.'-01-01 00:00:00';
                        $date_to           = $year.'-12-31 23:59:59';
                        $title_date_format = '%Y';


                        // Month and Year?
                        if (perch_get('month')) {
                            $month             = intval(perch_get('month'));
                            $date_from         = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-01 00:00:00';
                            $date_to           = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-31 23:59:59';
                            $title_date_format = '%B, %Y';
                        }

                        echo $yearmarkup;
                        PerchSystem::set_var('myurl', '/blog/'.$year.'/page/');
                        perch_blog_custom(array(
                                'page-links' => true,
                                'filter'     => 'postDateTime',
                                'match'      => 'eqbetween',
                                'value'      => $date_from.','.$date_to,
                                'template'   => $template,
                                'count'      => $posts_per_page,
                                'sort'       => $sort_by,
                                'sort-order' => $sort_order,
                                ));

                        $posts_displayed = true;
                    }
                    

                    /* --------------------------- DEFAULT: ALL POSTS --------------------------- */

                    if ($posts_displayed == false) {

                        // No other options have been used; no posts have been displayed yet.
                        // So display all posts.

                        echo '<h1>Archive</h1>';

                        perch_blog_custom(array(
                                'template'   => $template,
                                'count'      => $posts_per_page,
                                'sort'       => $sort_by,
                                'sort-order' => $sort_order,
                                ));

                    }

                ?>
            <div id="articleSideBar">
                <?php perch_blog_categories(array(
                    'sort' =>'catTitle',
                    'sort-order' => 'ASC',
                )); ?>
                <?php perch_blog_authors(); ?>
                <?php perch_blog_date_archive_years(); ?>
                <div class="boxed clearfix sidebarContent">
                    <h3>Latest Tweets</h3>
                    <?php perch_twitter_get_latest(array(
                        'twitter_id' => 'lapsafe',
                        'template'=>'twitter/blog_tweet.html',
                        'count'=>16,
                    )); ?>
                </div>
            </div>
        </div>
        
            <?php perch_layout('global/footer'); ?>


    </body>
</html>