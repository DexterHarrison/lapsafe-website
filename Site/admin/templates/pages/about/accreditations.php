<!DOCTYPE html>
<html>
    <head>
		<?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <?php perch_pages_breadcrumbs(); ?>
        
        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <h2><?php perch_content('Sub Title'); ?></h2>
                </div>
                <div class="twelve columns">
                    <?php perch_content('Body'); ?>
                </div>
                <div class="four columns">
                    <?php perch_content('ISO Logo'); ?>
                </div>
            </div>
        </div>

        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>