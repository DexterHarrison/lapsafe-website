<!DOCTYPE html>
<html>
    <head>
		    <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>

        <?php perch_layout('global/nav'); ?>

        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>

        <?php perch_pages_breadcrumbs(); ?>

        <div class="contentSidebar clearfix">
            <div class="content boxed">
                <div class="row">
                    <h2>Who we are</h2>
                    <?php perch_content('Who We Are Body Text'); ?>
                </div>
                <h2>What we do</h2>
                <?php perch_content('What We Do Body Text'); ?>
            </div>
            <div class="sidebar">
                <?php perch_content('Other About Pages'); ?>
            </div>
        </div>

        <?php perch_layout('global/footer'); ?>

    </body>

</html>
