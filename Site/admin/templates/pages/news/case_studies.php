<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <?php 
            perch_pages_breadcrumbs();
            perch_content_custom('Case Studies', array(
                'template'   => '_Case_Studies/Case_Study-List.html',
            ));
        ?>

        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>