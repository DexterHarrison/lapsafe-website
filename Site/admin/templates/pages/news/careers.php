<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <?php perch_pages_breadcrumbs(); ?>
        <div class="page careers">
            <div class="container">
                <div class="sixteen columns">
                    <?php perch_content('Careers Text'); ?>
                </div>
            </div>
        </div>
        
        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>