<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/head'); ?>
		<?php
            perch_collection('News', array(
                'template' => 'News/news_meta.html',
                'filter' => 'slug',
                'match' => 'contains',
                'value' => perch_get('slug'),
                'count' => 1,
            ));
        ?>
        <script type="text/javascript" src="/js/smallslider.min.js"></script>
        <script>
            $(function(){
                $("#slides").slidesjs({
                    width: 700,
                    height: 400,
                    navigation: false,
                    interval: 5000,
                    auto: true
                });
            });
        </script>
    </head>
    <body>
        <?php perch_layout('global/nav'); ?>

        <ul class="breadcrumbs nohead">
            <li class="home"><img src="/images/icons/home.jpg"></li>
            <li><a href="/news">News</li></a>
            <li>
                <?php
                    perch_collection('News', array(
                        'template' => 'News/news_title.html',
                        'filter' => 'slug',
                        'match' => 'contains',
                        'value' => perch_get('slug'),
                        'count' => 1,
                    ));
                ?>
            </li>
        </ul>

        <div id="articles" class="clearfix">
            <div id="articleList">
                <?php
                    perch_collection('News', array(
                        'template' => 'News/news_page.html',
                        'filter' => 'slug',
                        'match' => 'contains',
                        'value' => perch_get('slug'),
                        'count' => 1,
                    ));
                ?>
            </div>
            <div id="articleSideBar">
                <?php
                    perch_collection('News', array(
                        'template' => 'News/news_sidebar.html',
                        'filter' => 'slug',
                        'match' => 'neq',
                        'value' => perch_get('slug'),
                        'count' => 3,
                    ));
                ?>
                <div class="boxed clearfix sidebarContent blogItem">
                    <h3>Latest Tweets</h3>
                    <?php perch_twitter_get_latest(array(
                        'twitter_id' => 'lapsafe',
                        'template'=>'twitter/blog_tweet.html',
                        'count'=>4,
                    )); ?>
                </div>
            </div>
        </div>

        <?php perch_layout('global/footer'); ?>

    </body>
</html>
