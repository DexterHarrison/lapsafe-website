<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        
        <?php 
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            )); 
            perch_content_custom('Header');
        ?>
        
        <?php perch_pages_breadcrumbs(); ?>
        
        <div id="articles" class="clearfix">
            
            <?php
                PerchSystem::set_var('myurl', '/news/page/');
                perch_collection('News', array (
                    'template' => 'News/news_intro.html',
                    'sort' => 'date',
                    'sort-order' => 'DESC',
                    'count' => 5,
                    'paginate' => true
                ));
            ?>
            
            <div id="articleSideBar">
                <div class="clearfix sidebarContent">
                    <?php perch_content('Other News Pages'); ?>
                </div>
            </div>
            
        </div>
    
        
        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>