<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/head'); ?>
        <?php
            if (perch_get('slug')) {
                perch_content_custom('Press Releases', array(
                'template' => '_Press_Releases/Press_Release_Meta.html',
                'filter' => 'url_slug',
                'match' => 'eq',
                'value' => perch_get('slug'),
                'count' => 1,
            ));
        } else { perch_page_attributes(); }

        ?>
		<?php perch_layout('global/css'); ?>
    </head>

    <body>
        <?php perch_layout('global/nav'); ?>

         <?php
            perch_content_create('Press Releases', array(
                'template'=>'_Press_Releases/Press_Release.html',
                'multiple'=>true,
                'edit-mode'=>'listdetail',
            ));
        ?>

        <?php
            if (perch_get('slug')) {
                perch_content_custom('Press Releases', array(
                    'template' => '_Press_Releases/Press_Release.html',
                    'filter' => 'url_slug',
                    'match' => 'eq',
                    'value' => perch_get('slug'),
                    'count' => 1,
                ));
            }

            else {
                perch_content_create('Header', array(
                    'template' => 'General/_Title_Header.html',
                ));
                perch_content_custom('Header');
                perch_pages_breadcrumbs();
                perch_content_custom('Press Releases', array(
                    'template'   => '_Press_Releases/Press_Release-List.html',
					'sort'=>'date',
        			'sort-order'=>'DESC',
                ));
            }
        ?>

        <?php perch_layout('global/footer'); ?>

    </body>

</html>
