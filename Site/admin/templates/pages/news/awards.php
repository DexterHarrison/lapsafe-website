<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>

    <body>

        <?php perch_layout('global/nav'); ?>

        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>

        <?php perch_pages_breadcrumbs(); ?>

        <div class="page awards">
            <div class="container">
                <div class="sixteen columns awards-intro">
                    <h2><?php perch_content('Sub Title'); ?></h2>
                </div>

                <div class="sixteen columns row">
                    <?php perch_content('Awards Text'); ?>
                </div>

                <?php perch_gallery_album_images('awards', array(
                    'template' => 'lightbox.html',
                )); ?>

            </div>
        </div>

        <?php perch_layout('global/footer'); ?>
        <script type="text/javascript" src="/js/lightbox.min.js"></script>
    </body>

</html>
