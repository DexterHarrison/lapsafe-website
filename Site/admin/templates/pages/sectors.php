<!DOCTYPE html>
<html>
    <head>
		<?php perch_layout('global/masthead'); ?>
    </head>
    
    <body class="sectors">
        
        <?php perch_layout('global/nav'); ?>
        
        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>
        
        <?php perch_pages_breadcrumbs(); ?>
        <div class="page">
        
            <?php perch_categories(array(
                    'set'=>'sectors',
                    'template'=>'sector.html',
                )
            ); ?>
            
        </div>
        
        <?php perch_layout('global/footer'); ?>
        
    </body>
</html>