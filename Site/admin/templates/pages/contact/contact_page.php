<?php

    $nameErr = $orgErr = $emailErr = $phoneErr = $notesErr = $formErr = $capErr = NULL;
    $name = $org = $email = $phone = $product = $notes = $cap = NULL;

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		function getUserIP() {
			if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
				if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
					$addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
					return trim($addr[0]);
				} else {
					return $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
			}
			else {
				return $_SERVER['REMOTE_ADDR'];
			}
		}
		$ip = getuserIP();
                        
        $valid = true;
                
        if(empty($_POST['name'])) {
            $nameErr = "<label class='error'>Please enter your name</label>";
            $nameClass = "class='error'";
            $valid = false;
        } else {
            $name = $_POST['name'];
        }
        
        if(empty($_POST['org'])) {
            $orgErr = "<label class='error'>Please enter your Organisation</label>";
            $orgClass = "class='error'";
            $valid = false;
        } else {
            $org = $_POST['org'];
        }
        
        if(empty($_POST['email'])) {
            $emailErr = "<label class='error'>Please enter an email address</label>"; // SET AN ERROR MESSAGE FOR MISSING EMAIL ADDRESS
            $emailClass = "class='error'";
            $valid = false; // MISSING EMAIL ADDRESS INVALIDATE FORM
        } elseif (filter_var(($_POST['email']), FILTER_VALIDATE_EMAIL)) {
            $email = ($_POST['email']); // EMAIL IS VALID SET SESSION VARIABLES
        } else {
            $emailErr = "<label class='error'>Please enter a valid email</label>"; // SET AN ERROR MESSAGE EMAIL WAS PRESENT BUT INVALID
            $emailClass = "class='error'";
            $valid = false; // INVALID EMAIL INVALIDATE FORM
        }
        
        if(!empty($_POST['phone'])) {
            
            if (preg_match('/^\+?\d+$/', ($_POST['phone']))) {
                $phone = $_POST['phone']; // PHONE NUMBER CONSISTS OF ONLY NUMBERS AND A '+' AT THE START SO IT PASSES VALIDATION
            } else {
                $phoneErr = "<label class='label'>Phone number must only contain numbers</label>"; // INVALID PHONE NUMBER (NON NUMERIC CHARS ENTERED)
                $phoneClass = "class='error'";
                $valid = false; // INVALID PHONE
            }
        } else {
            $phone = NULL;
        }
        
        if(empty($_POST['notes'])) {
            $notesErr = "<label class='error'>Please enter a message</label>";
            $notesClass = "class='error'";
            $valid = false;
        } else {
            $notes = $_POST['notes'];
        }
        
        if(isset($_POST['g-recaptcha-response'])){
          $captcha=$_POST['g-recaptcha-response'];
        }
        
        if(!$captcha){
            $capErr = "<label class='error'>Please tick the captcha</label>";
            $valid = false;
        }
        
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Le7g-8SAAAAAHcSd4gCoalsCCVRKpKX3Lc8sFER&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        if($response.success==false)
        {
          $capErr = "<label class='error'>Please tick the captcha</label>";
            $valid = false;
        }
        if($valid) {
            require_once($_SERVER['DOCUMENT_ROOT'].'/includes/forms/enquiry/submission.php');
        } else {
            $formErr = "There are errors on the form please review these";
        }
        
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <style>
            input[type="submit"]:disabled {
                background: green !important;   
            }
        </style>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        
        <section class="parallaxHeader small contact">
            <div id="googlemaps">
            </div>
            <h1>Contact Us</h1>
        </section>
        
        <?php perch_pages_breadcrumbs(); ?>
        
        <div class="floatContainer">
            <div class="floatTwo boxed contactForm">
                <div class="title">
                    <h2>Enquiry</h2>
                </div>
                <form method="post" action="/contact-us/">
                    <div class="field">
                        <label for="name">Name*</label>
                        <input type="text" name="name" value="<?php echo $name ?>" <?php echo $nameClass; ?>>
                        <?php echo $nameErr; ?>
                    </div>

                    <div class="field">
                        <label for="org">Company Name*</label>
                        <input type="text" name="org" value="<?php echo $org ?>" <?php echo $orgClass; ?>>
                        <?php echo $orgErr; ?>
                    </div>

                    <div class="field">
                        <label for="email">Email*</label>
                        <input type="text" name="email" value="<?php echo $email ?>" <?php echo $emailClass; ?>>
                        <?php echo $emailErr; ?>
                    </div>

                    <div class="field">
                        <label for="phone">Phone Number</label>
                        <input type="text" name="phone" value="<?php echo $phone ?>" <?php echo $phoneClass; ?>>
                        <?php echo $phoneErr; ?>
                    </div>

                    <div class="field">
                        <label for="notes">Message*</label>
                        <textarea name="notes" <?php echo $notesClass; ?>><?php echo htmlspecialchars($notes); ?></textarea>
                        <?php echo $notesErr; ?>
                    </div>

                    <div class="field">
                        <div class="g-recaptcha" data-sitekey="6Le7g-8SAAAAAAjMT6q4AMYx4OdilaOg9C8myr-d"></div>
                        <?php echo $capErr; ?>
                    </div>

                    <input type="submit" id="submit" value="Send">
                </form>

            </div>
            <div class="floatTwo boxed contactForm">
                <div class="title">
                    <h2>Our Details</h2>
                </div>

                <div class="normalContact">
                    <div class="address borderBottom">
                        <ul>
                            <li><strong>LapSafe® Products Ltd</strong></li>
                            <li>Unit 3 Wakes Hall Business Centre</li>
                            <li>Wakes Colne</li>
                            <li>Colchester</li>
                            <li>Essex</li>
                            <li>CO6 2DY</li>
                        </ul>
                    </div>
                    <div class="contactDetails borderBottom">
                        <ul>
                            <li><strong>Freephone</strong><br>0800 130 3456</li>
                            <li><strong>Email</strong><br>sales@lapsafe.com</li>
                            <li><strong>Fax</strong><br>01787 226 267</li>
                        </ul>
                    </div>
                    <div class="socialDetails borderBottom">
                        <ul>
                            <li><a href="">Follow us on Twitter</a></li>
                            <li><a href="">Like us on FaceBook</a></li>
                            <li><a href="">Follow us on LinkedIn</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

            <?php perch_layout('global/footer'); ?>
            <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
            <script type="text/javascript">
 
                // The latitude and longitude of your business / place
                var position = [51.9259644, 0.7428532999999788];
 
                    function showGoogleMaps() {
 
                    var latLng = new google.maps.LatLng(position[0], position[1]);

                    var mapOptions = {
                        zoom: 12, // initialize zoom level - the max value is 21
                        streetViewControl: true, // hide the yellow Street View pegman
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        center: latLng
                };
 
                    map = new google.maps.Map(document.getElementById('googlemaps'),
                        mapOptions);

                    // Show the default red marker at the location
                    marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP
                    });
                }
 
                google.maps.event.addDomListener(window, 'load', showGoogleMaps);
            </script>
        
    </body>
    
</html>