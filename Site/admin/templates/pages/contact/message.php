<!DOCTYPE html>
<html>
    <head>
        <?php perch_layout('global/masthead'); ?>
    </head>
    
    <body>
        
        <?php perch_layout('global/nav'); ?>
        <?php
            perch_content_create('Header', array(
                'template' => 'General/_Title_Header.html',
            ));
            perch_content_custom('Header');
        ?>
        
        <div class="page">
            <div class="container">
                <div class="sixteen columns">
                    <?php perch_content_create('Messages', array(
                        'template' => 'General/_Message.html',
                    )); ?>
                    
                    <?php 
                        perch_content_custom('Messages', array(
                            'count' => 1,
                            'filter' => 'slug',
                            'match' => 'eq',
                            'value' => perch_get('m')
                        ));
                    ?>
                    <?php 
                        if(!perch_get('m')) {
                            echo "<h2>You've arrived here unexpectedly</h2>";
                            echo "<p>If you would like to <a href='/contact-us'>contact us please visit here</a> or call us on <strong>0800 130 3456</strong></p>";
                        } 
                    ?>
                </div>
            </div>
        </div>            
    
        <?php perch_layout('global/footer'); ?>
        
    </body>
    
</html>