    <div class="container">
        <div class="sixteen columns">
            <h2>Contact us</h2>
        </div>
        <form action="/contact-us/" method="post">
            <div class="eight columns">
                <label for="name">Full Name</label><br /><input id="name" type="text" value="" name="name" />
                <label for="name">Company Name</label><br /><input id="company" type="text" value="" name="org" />
                <label for="email">E-mail</label><br /><input id="email" type="text" value="" name="email" />                        
                <label for="phone">Phone</label><br /><input id="phone" type="tel" value="" name="phone" />
            </div>
            <div class="eight columns">
                <label for="message">Message</label><br /><textarea id="message" rows="7" cols="30" name="notes"></textarea>
            </div>
            
            <div class="sixteen columns">
                <div class="g-recaptcha" data-sitekey="6Le7g-8SAAAAAAjMT6q4AMYx4OdilaOg9C8myr-d"></div>
                <input class="submit" type="submit" name="submit" value="Submit Enquiry" />
            </div>
        </form>
    </div>