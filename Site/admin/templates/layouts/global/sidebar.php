<nav id="social-sidebar">
    <ul>
        <li>
            <a class="facebook" href="">
                <i class="fa fa-facebook"></i>
                <span>Facebook</span>
            </a>
        </li>
        <li>
            <a class="twitter" href="">
                <i class="fa fa-twitter"></i>
                <span>Twitter</span>
            </a>
        </li>
        <li>
            <a class="google" href="">
                <i class="fa fa-google"></i>
                <span>Google +</span>
            </a>
        </li>
        <li>
            <a class="linkedin" href="">
                <i class="fa fa-linkedin"></i>
                <span>LinkedIn</span>
            </a>
        </li>
        <li>
            <a class="youtube" href="">
                <i class="fa fa-youtube"></i>
                <span>YouTube</span>
            </a>
        </li>
    </ul>
</nav>