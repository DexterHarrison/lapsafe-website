<div id="progress">
    <div id="bar"></div>
</div>
<header>
            <nav class="lapsafe-menu">
                <label for="mobile-button"> <i class="fa fa-bars"></i> </label>
                <input id="mobile-button" type="checkbox">
                    <ul class="collapse">
                        <span class="mobile-logo">LapSafe® Products</span>
                        <li><a href="/">Home</a></li>
                        
                        <li> <a href="/about-us">About</a>
                            <ul class="drop-down one-column hover-fade">
                                <li><a href="/about-us">Company Profile</a></li>
                                <li><a href="/about-us/meet-the-team">Meet the team</a></li>
                                <li><a href="/about-us/accreditations">Accreditations</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="/products">Products</a>
                            <ul class="drop-down one-column hover-fade">
                                <li><a href="/products/mobile-charging">Mobile Storage & Charging</a><i class="fa fa-angle-right"></i>
                                    <ul class="drop-down one-column hover-fade">
                                        <li><a href="/products/mobile-charging/classbuddy">ClassBuddy</a></li>
                                        <li><a href="/products/mobile-charging/unocart">UnoCart</a></li>
                                        <li><a href="/products/mobile-charging/mentor">Mentor</a></li>
                                        <li><a href="/products/mobile-charging/traveller/">Traveller</a><i class="fa fa-angle-right"></i>
                                            <ul class="drop-down one-column hover-fade">
                                                <li><a href="/products/mobile-charging/traveller/charge-and-sync">Traveller Tablet - Charge & Sync</a></li>
                                                <li><a href="/products/mobile-charging/traveller/universal">Traveller Universal</a></li>
                                                <li><a href="/products/mobile-charging/traveller/emini">Traveller eMini</a></li>
                                                <li><a href="/products/mobile-charging/traveller/smartline">Traveller SmartLine</a></li>
                                                <li><a href="/products/mobile-charging/traveller/dsi">Traveller DSi</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="/products/mobile-charging/icshub">iCSHub</a></li>
                                    </ul>
                                </li>
                                <li><a href="/products/fixed-charging">Fixed Storage & Charging</a><i class="fa fa-angle-right"></i>
                                    <ul class="drop-down one-column hover-fade">
                                        <li><a href="/products/fixed-charging/diplomat">Diplomat Range</a><i class="fa fa-angle-right"></i>
                                            <ul class="drop-down one-column hover-fade">
                                                <li><a href="/products/fixed-charging/diplomat/lms">Diplomat LMS</a></li>
                                                <li><a href="/products/fixed-charging/diplomat/loxs">Diplomat with Nedap LoXS</a></li>
                                                <li><a href="/products/fixed-charging/diplomat/pin">Diplomat PIN</a></li>
                                                <li><a href="/products/fixed-charging/diplomat/cam">Diplomat CAM</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="/products/fixed-charging/solo-plus-plus">Solo Plus +</a></li>
                                    </ul>
                                </li>
                                <li><a href="/products/solutions-for">Solutions for</a><i class="fa fa-angle-right"></i>
                                    <ul class="drop-down one-column hover-fade">
                                        <li><a href="/products/solutions-for/tablets">Tablets</a></li>
                                        <li><a href="/products/solutions-for/laptops">Laptops</a></li>
                                        <li><a href="/products/solutions-for/chromebooks">Chromebooks</a></li>
                                    </ul>
                                </li>
                                <li><a href="/brochure">Request a Brochure</a></li>
                                <li><a href="/find">Find the Perfect Product for Me</a></li>
                            </ul>
                        </li>
                        
                        <li class="drop"><a href="/sectors">Sectors</a>
                            <ul class="drop-down one-column hover-fade">
                                <li><a href="/sectors/#education">Education</a></li>
                                <li><a href="/sectors/#aviation">Aviation</a></li>
                                <li><a href="/sectors/#hotels">Hotels & Leisure</a></li>
                                <li><a href="/sectors/#healthcare">Healthcare</a></li>
                                <li><a href="/sectors/#public-sector">Public Sector</a></li>
                            </ul>
                        </li>
                                                
                        <li class="logo">
                            <img class="navLogo" src="/images/logos/white-logo.svg" alt="LapSafe Products Charge & Sync">
                            <img class="ieLogo" src="/images/logos/white-logo.png" alt="LapSafe Products Charge & Sync">
                        </li>
                        
                        <li class="drop"><a href="/news/case-studies">Case Studies</a></li>
                        
                        <li><a href="/news/">News</a>
                            <ul class="drop-down one-column hover-fade">
                                <ul>
                                    <li><a href="/news/exhibitions">Events & Exhibitions</a></li>
                                    <li><a href="/blog">Blog</a></li>
                                    <li><a href="/news/case-studies">Case Studies</a></li>
                                    <li><a href="/news/press-releases">Press Releases</a></li>
                                    <li><a href="/news/awards">Awards</a></li>
                                    <li><a href="/news/careers">Careers</a></li>
                                </ul>
                            </ul>
                        </li>
                        
                        <li><a href="/support">Support</a>
                            <ul class="drop-down one-column hover-fade">
                                <li><a href="/support/order-keys">Order Keys</a></li>
                                <li><a href="/support/upgrade">Upgrade</a></li>
                                <li><a href="/support/enquiry">Support Enquiry</a></li>
                                <li><a href="/support/activate-warranty">Activate Warranty</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="/contact-us">Contact</a>
                            <ul style="right: 0;" class="drop-down one-column hover-fade">
                                <li><a href="/contact-us">Get in Touch</a></li>
                                <li><a href="/support/enquiry">Support Enquiry</a></li>
                                <li><a href="/support/upgrade">Upgrade</a></li>
                                <li><a href="/contact-us/book-a-demo">Book a demo</a></li>
                                <li><a href="/contact-us/become-a-reseller">Become a reseller</a></li>
                                <li><a href="/brochure">Request a brochure</a></li>
                            </ul>
                        </li>
                    </ul>
				</nav>
            </header>