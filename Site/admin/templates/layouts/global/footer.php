<footer>
    <div class="message">
        <?php
            perch_content_custom('Notices', array(
                'count' => 1,
                'sort-order' => 'RAND',
                'sort'=>'message',
            ));
        ?>
    </div>
    <div class="content">
        <div class="container others">
            <div class="three columns">
                <span class="title">
                    Follow us
                </span>
                <ul>
                    <li>
                        <a target="_blank" href="https://www.facebook.com/LapSafeProducts"><i class="fa fa-facebook"></i> Facebook</a>
                    </li>
                    <li>
                        <a target="_blank" href="https://twitter.com/lapsafe"><i class="fa fa-twitter"></i> Twitter</a>
                    </li>
                    <li>
                        <a target="_blank" href="https://plus.google.com/+Lapsafe/posts"><i class="fa fa-google"></i> Google+</a>
                    </li>
                    <li>
                        <a target="_blank" href="https://www.linkedin.com/company/lapsafe-products"><i class="fa fa-linkedin"></i> LinkedIn</a>
                    </li>
                    <li>
                        <a target="_blank" href="https://www.youtube.com/user/LapSafeProducts"><i class="fa fa-youtube"></i> YouTube</a>
                    </li>
                </ul>
            </div>
            <div class="three columns">
                <span class="title">
                    Helpful links
                </span>
                <ul>
                    <li><a href="/brochure">Request a brochure</a></li>
                    <li><a href="/support/order-keys">Replacement Keys</a></li>
                    <li><a href="/support/enquiry">Support Enquiry</a></li>
                    <li><a href="/news/case-studies">Case Studies</a></li>
                    <li><a href="/news/press-releases">Press Releases</a></li>
                    <li><a href="/about-us/meet-the-team">Meet The Team</a></li>
                </ul>
            </div>
            <div class="five columns">
                <span class="title">
                    Latest Blog Posts
                </span>
                <?php 
                    perch_blog_custom(array(
                        'count' => 5,
                        'template' => 'footer.html',
                        'sort' => 'postDateTime',
                        'sort-order' => 'DESC',
                    )); 
                ?>
            </div>
            <div class="five columns">
                <span class="title">
                    Keep up to date with our newsletter
                </span>
                <?php perch_mailchimp_form('subscribe.html'); ?>
                
            </div>
        </div>
        <div class="container copyright">
            &copy; Copyright LapSafe&reg; Products Ltd <?php echo date("Y"); ?> - <a style="color: white;" href="//www.iubenda.com/privacy-policy/623080" class="iubenda-nostyle no-brand iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
        </div>
    </div>
</footer>

<script>
$(window).scroll(function() {

    var s = $(window).scrollTop();
    var h = $(document).height();
    var w = $(window).height();

    var maxh = h - w;

    var p = (s / maxh) * 100;
    
    $('#bar').width(p + '%');
});
</script>