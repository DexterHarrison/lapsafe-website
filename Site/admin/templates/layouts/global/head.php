<meta charset="utf-8"><!-- UTF8 -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- View Port -->
<script src="//use.typekit.net/qyy7grn.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<link rel="shortcut icon" href="/images/icons/browser/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="/images/browser/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="57x57" href="/images/icons/browser/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/icons/browser/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/icons/browser/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/icons/browser/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/icons/browser/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/icons/browser/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/icons/browser/apple-touch-icon-152x152.png">
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-687177-1', 'lapsafe.com');
	ga('require', 'displayfeatures');
	ga('send', 'pageview');
</script><!-- Google Analytics -->
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="http://web-path.com/js/5955.js" async></script> <noscript><img src="http://web-path.com/images/track/5955.png?trk_user=5955trk_tit=jsdisabledtrk_ref=jsdisabledtrk_loc=jsdisabled" height="0px" width="0px" style="display:none;" /></noscript>
<link rel="stylesheet" href="/css/style.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="/js/selectivizr-min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/css/ie.css">
<![endif]-->
