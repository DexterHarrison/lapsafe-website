<?php perch_layout('global/head'); ?>
<?php 
    $domain        = 'http://'.$_SERVER["HTTP_HOST"];
    $url           = $domain.$_SERVER["REQUEST_URI"];
    $sitename      = "LapSafe Products - Store, Charge & Sync";
    $twittername   = "@lapsafe";
    $sharing_image = '/admin/resources/open-graph/lapsafe-range-w1200.jpg';

    perch_page_attributes_extend(array(
        'sitename'       => $sitename,
        'domain'         => $domain,
        'url'            => $url,
        'sharing_image'  => $sharing_image,
        'twittername'    => $twittername
    ));

    perch_page_attributes(array(
        'template'      => 'default.html'
    ));
?>